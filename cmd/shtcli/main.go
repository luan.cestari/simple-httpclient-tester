package shtcli

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	log.Println("starting the application")

	var signalChannel = make(chan os.Signal)
	signal.Notify(signalChannel, syscall.SIGTERM)
	signal.Notify(signalChannel, syscall.SIGINT)

	var httpClientStop = make(chan bool)
	var makeShutdownRequest = make(chan bool)

	go gracefulShutdown(signalChannel, httpClientStop, makeShutdownRequest)

	var timeout = 5 * time.Second

	go HTTPClientGetResponseTimeForURL(timeout, "https://gitlab.com", httpClientStop, makeShutdownRequest)

	<-makeShutdownRequest
	makeShutdownRequest <- true

	//Give some time to the goroutines communicate
	time.Sleep(100 * time.Millisecond)
}
