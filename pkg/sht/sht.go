package sht

import (
	"log"
	"net"
	"net/http"
	"os"
	"time"
)

// gracefulShutdown handle process signal to shutdown, giving 1 second to the goroutines finish their job. Could use a external dependecy or even make make a project only for this
func gracefulShutdown(osSignal chan os.Signal, informShutdown chan bool, receiveShutdownRequest chan bool) {
	select {
	case osSig := <-osSignal:
		log.Printf("the OS sent signal '%+v' and the program will shutdown in 1 second\n", osSig)
		informShutdown <- true
		time.Sleep(1 * time.Second)
		os.Exit(0)
	case <-receiveShutdownRequest:
		log.Printf("a goroutine sent signal to stop and the program will shutdown in 1 second\n")
		informShutdown <- true
		time.Sleep(1 * time.Second)
		os.Exit(0)
	}

}
func elapedTimeTrack(start time.Time) {
	elapsed := time.Since(start)
	log.Printf("elapsed time %s for the request", elapsed)
}

func makeHTTPCall(httpClient *http.Client, url string) {
	defer elapedTimeTrack(time.Now())
	_, err := httpClient.Get(url)
	if err != nil {
		log.Fatal(err)
	}
}

// HTTPClientGetResponseTimeForURL Will check the HTTP response time over a specifed time and for the URL informed, using a channel to stop before timeout
func HTTPClientGetResponseTimeForURL(timeout time.Duration, url string, httpClientStop chan bool, makeShutdownRequest chan bool) {
	timeoutAfter := time.After(timeout)

	//The timeouts could be handled in case it is required. I set them to change from the default timeout which would wait the request indefinitely
	var netTransport = &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 10 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}
	var netClient = &http.Client{
		Timeout:   time.Second * 15,
		Transport: netTransport,
	}

	for {
		select {
		case <-timeoutAfter:
			log.Println("terminating due timeout reached")
			makeShutdownRequest <- true
			return
		case <-httpClientStop:
			log.Println("terminating due a goroutine send a signal to stop")
			return
		default:
			makeHTTPCall(netClient, url)
		}
	}
}
