package sht

import (
	"testing"
	"time"
)

func TestHTTPClientGetResponseTimeForURL(t *testing.T) {
	type args struct {
		timeout             time.Duration
		url                 string
		httpClientStop      chan bool
		makeShutdownRequest chan bool
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "#1 - Test",
			args: args{
				timeout:             2 * time.Second,
				url:                 "https://gitlab.com",
				httpClientStop:      make(chan bool),
				makeShutdownRequest: make(chan bool),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			go HTTPClientGetResponseTimeForURL(tt.args.timeout, tt.args.url, tt.args.httpClientStop, tt.args.makeShutdownRequest)
			<-tt.args.makeShutdownRequest
		})
	}
}
